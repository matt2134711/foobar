CFLAGS ?= -O2 -g -Wall -Wextra
CFLAGS += -std=c11
CFLAGS += -MMD -MP

OBJECTS := \
	bar.o \
	foo.o \
	main.o

.PHONY: all clean
all: foobar

foobar: $(OBJECTS)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

clean:
	$(RM) *.d *.o foobar

-include $(OBJECTS:.o=.d)
