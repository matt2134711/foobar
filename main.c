#include <stdio.h>
#include <stdlib.h>

#include "bar.h"
#include "foo.h"

int main() {
	printf("%s, %s!\n", foo_get_hello(), bar_get_world());
	return EXIT_SUCCESS;
}
